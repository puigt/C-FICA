function perm=permutations(xc,sc)

% Identification of the source permutations between the true and estimated 
% contributions tensors xc and sc
%
% Inputs
% xc : tensor whose element xc(i,j,:) is the true contribution of the j^th
% source in the i^th sensor
% sc : tensor whose element sc(i,j,:) is the estimated contribution of the 
% j^th permutated source in the i^th sensor
%
% Ouput
% perm : permutation vector

for r=1:size(xc,2)
    
s1(:,1,:)=xc(:,r,:);
s2(:,:,:)=sc(:,:,:);

perm(r)=ident(s1,s2);

end

%--------------------------------------------------------------------------

function [i]=ident(s1,s2)

for r=1:size(s2,2)
    sir1(r)=0;
    for k=1:size(s1,1);
        sir1(r)=max(sir(s1(k,1,:),s2(k,r,:)),sir1(r));
    end
end

[SIR,i]=max(sir1);

%--------------------------------------------------------------------------

function sir1=sir(s1,s2)

sir1=10*log10(mean(s1.^2)/mean((s1-s2).^2));

