These HRTF files are taken from:

> B. Gardner and K. Martin. Head Related Transfer Functions of a Dummy Head
> [ica-bench](http://sound.media.mit.edu/ica-bench)