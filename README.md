# Overview

The C-FICA algorithm (Convolutive extension of FastICA) is a time-domain fast fixed-point algorithm that realizes blind source separation of convolutive mixtures. It is based on a convolutive sphering process (or spatio-temporal sphering) that lets the use of the classical FastICA updates to extract iteratively the innovation processes of the sources in a deflation procedure. For the estimation of the source contributions, we use a least-square criterion whose optimization is realized by a Wiener filtering process. The cfica.m function implements our algorithm in Matlab language and proposes different parameters and options. One can choose for instance the orders of the extraction and recoloration filters, the non-Gaussianity criterion or the windows on which the extraction and recoloration filters are estimated (see below in the Matlab help for details). The main output is the signal vector ’Sc’ whose raws are some estimated contributions of the different sources (the most powerful contribution for each considered source). Some other outputs are available as the estimated filters or the innovation processes for instance. Our algorithm ideally works with MA (Moving Average) mixtures of MA sources because of the relevance proof that is made by mapping the mixtures into linear instantaneous ones. Nevertheless, with real speech sources, we obtain notable separation results by choosing adequately the window of extraction (see demo2).

We provide in this package two demo programs : the first one "demo1.m" realizes the separation of artificial colored sources mixed with real filters recorded at the ears of a dummy head. The user can change the directions of arrival of the two source signals. The SIRs of the estimated contributions are computed and some figures compare the estimated contributions with the true ones.
The second demo "demo2.m" uses real speech signals and mixes them with the same set of filters as above. The extraction of the innovation process is here made on a small-size window that has been correctly chosen (we are currently working on a automatic choice of this window). By choosing 0 as order of the extraction and recoloration filters, our function cfica also separates linear instantaneous mixtures.



# cfica Matlab help :
 
``` 
C-FICA Algorithm - Convolutive extension of FastICA

[Sc, sc, Isc, y, fe, fc, te, tc]=cfica(x, non-lin, epsi, Re, [we1,we2], Rc, [wc1,wc2], itermax, init, verbose)
 
Inputs (place the deconvolution parameters before the recoloration ones themselves before iter_max) :

* x : observation vector
* non-lin : non-linearity, ’k’>kurtosis, ’g’>gaussian, ’t’>tanh (by default ’g’)
* init : ’i’ to initialize with unit filters (optional)
* epsi : stopping criterion (between 0 and 1) default 10^-12
* Re : order of the extraction filters (default 80)
* [we1,we2] : extraction time window (optional)
* Rc : order of the recoloration filters (default 3R1)
* [wc1,wc2] : recoloration time window (optional)
* itermax : maximum number of iterations (default 10000)
* verbose : ’v’ for verbose (optional)

Outputs :

* Sc : most powerful contributions for each estimated source
* sc : estimated source contributions (tensor N*N*nb_samples)
* Isc : indices of the most powerful contributions
* y : N-1 estimated innovation processes (matrix N*nb_samples)
* fe : extraction filters
* fc : recoloration filters
* te : times of extraction
* tc : times of recoloration

```

# About this version
The original C-FICA v1.1 code was written by J. Thomas. Unfortunately, it was providing an error message because of some changes in the way to process tensors in recent Matlab releases. The current version (named C-FICA v1.2) corrects this bug and is maintained by Matthieu PUIGT (Matthieu [dot] Puigt [at] univ-littoral [dot] fr) and Yannick DEVILLE (Yannick [dot] Deville [at] irap [dot] omp [dot] eu).

Please acknowledge the use of this software in any publication: "The C-FICA software is available at https://gogs.univ-littoral.fr/puigt/C-FICA" and cite the reference :
> J. Thomas, Y. Deville, S. Hosseini, Time-domain fast fixed-point algorithms for convolutive ICA, IEEE Signal Processing Letters, vol. 13, no. 4, pp. 228-231, April 2006.