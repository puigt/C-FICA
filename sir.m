% Computation of the Signal to Interferences Ratio between the reference
% signal s1 and its estimation s2.
%
% Inputs
% s1 : reference signal
% s2 : estimated signal
%
% Output
% SIR : Signal to Interferences Ratio between s1 and s2 (dB)

function SIR=sir(s1,s2)

SIR=10*log10(mean(s1.^2)/mean((s1-s2).^2));
