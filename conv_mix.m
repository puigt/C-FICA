function x=conv_mix(A,s);

% Convolutive mixing process - V1.2
%
% Inputs
% A : mixing tensor whose A(i,j,:) element is the filter associated to the
% j^th source and to the i^th observation
% s : source vector
%
% Output
% x : observation vector
%
% -------------------------------------------------------------------------
% History of the software:
% V1.1: original software written by J. Thomas
%
% V1.2: Error messages appeared with recent releases of Matlab in the function
% conv_mix. This version corrects them. 
% Author: Matthieu PUIGT   Contact: matthieu.puigt@univ-littoral.fr
%
% -------------------------------------------------------------------------

x=zeros(size(A,1),size(s,2));

for i=1:size(A,1)
    for j=1:size(s,1)
        x(i,:)=x(i,:)+filter(reshape(A(i,j,:),size(A,3),1,1),1,s(j,:));
    end
end
