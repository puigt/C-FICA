% Generation of Laplacian random i.i.d. processes
%
% Inputs
% N : number of sources
% T : number of samples

% Outputs
% s : source vector

function s=laplace(N,T);

TAU=1;

s0=rand(N,T);
r1=(s0<=0.5).*(log(2*s0)/TAU);
r2=(s0>0.5).*(-log(2-2*s0)/TAU);
s=r1+r2;

s=cnorm(s);

function s=cnorm(e)

for k=1:size(e,1);
s(k,:)=e(k,:)-mean(e(k,:));
s(k,:)=s(k,:)./mean(s(k,:).^2)^0.5;
end

