function [xc]=contributions(A,s)

% Computation of the true contributions - V1.2
%
% Inputs
% A : mixing tensor whose A(i,j,:) element is the filter associated to the
% j^th source and to the i^th observation
% s : source vector
%
% Output
% xc : tensor whose xc(i,j,:) element is the true contribution of the j^th
% source in the i^th observation
% -------------------------------------------------------------------------
% History of the software:
% V1.1: original software written by J. Thomas
%
% V1.2: Error messages appeared with recent releases of Matlab in the function
% contributions. This version corrects them. 
% Author: Matthieu PUIGT   Contact: matthieu.puigt@univ-littoral.fr
%
% -------------------------------------------------------------------------


for i=1:size(A,1)
    for j=1:size(A,2)
        xc(i,j,:)=filter(reshape(A(i,j,:),size(A,3),1,1),1,s(j,:));
    end
end
