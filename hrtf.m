% Load HRTF (Head Related Transfer Functions) for a given azimuth.
% B. Gardner and K. Martin. Head Related Transfer Functions of a Dummy Head
% http://sound.media.mit.edu/ica-bench/.

function f = hrtf( az)

% Bring in 0-360 degrees region
while az > 360
	az = az - 360;
end
while az < 0
	az = az + 360;
end

% 180+x = flipped x
flip = 0;
if az > 180 
	az = az - 180;
	flip = 1;
end

% Load filters
fname = sprintf( 'hrtf/H0e%.3da.dat', az);
fid = fopen( fname, 'r', 'ieee-be');
if fid == -1
	error( sprintf( 'Cannot open file %s', fname));
end
tmp = fread( fid, inf, 'short');
fclose( fid);

% Assign to output and scale
% filters are at 44100, decimate to the default 22050
if ~flip
	f = [decimate( tmp(1:2:end), 2)' ; decimate( tmp(2:2:end), 2)']/32767;
else
	f = [decimate( tmp(2:2:end), 2)' ; decimate( tmp(1:2:end), 2)']/32767;
end
