% Demonstration program of the C-FICA algorithm used with real speech
% signals and real filters

clear all; clc; close all;

% Sources definition fs=20e3
s(1,:)=wavread('speech1');
s(2,:)=wavread('speech2');

% Definition of the filter tensor by using the following data base:
% B. Gardner and K. Martin. Head Related Transfer Functions of a Dummy Head
% http://sound.media.mit.edu/ica-bench/.
A(:,1,:)=hrtf(-10);
A(:,2,:)=hrtf(40);

% Convolutive mixing process
x=conv_mix(A,s);

% C-FICA algorithm (we choose [63000 63500] as an extraction window; we are 
% currently working on an automatic choice of this window)
[Sc,sc,Isc]=cfica(x,1e-16,[63000 63500],80,400,'v');

% Computation of the true contributions
xc=contributions(A,s);

% Identification of the source permutations
perm=permutations(xc,sc);
sc=sc(:,perm,:);Isc=Isc(:,perm);

% SIRs computation
SIR1=sir(xc(Isc(1),1,:),sc(Isc(1),1,:))
SIR2=sir(xc(Isc(2),2,:),sc(Isc(2),2,:))

% Plotting of the true (blue) and estimated (red) contributions
figure
hold on
plot(squeeze(xc(Isc(1),1,:)));
plot(squeeze(sc(Isc(1),1,:)),'r');

figure
hold on
plot(squeeze(xc(Isc(2),2,:)));
plot(squeeze(sc(Isc(2),2,:)),'r');

